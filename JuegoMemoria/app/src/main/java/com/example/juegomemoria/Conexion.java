package com.example.juegomemoria;

import java.sql.*;
import java.util.ArrayList;

public class Conexion {

    private ResultSet rs;
    private Statement st;
    private Connection conexionMySQL;
    private ArrayList <String> nombres;
    private ArrayList <Integer> movimientos;
    private ArrayList <String> dificultades;
    private ArrayList <String> datosJugador;
    private Boolean devuelveDatos;

    public Boolean ejecutarSQL(String sql) {
        devuelveDatos = false;

        nombres = new ArrayList<>();
        movimientos = new ArrayList<>();
        dificultades = new ArrayList<>();

        new Thread(() -> {
           try {
                conexionMySQL = DriverManager.getConnection("jdbc:mysql://192.168.1.129:3306/ranking", "ivan", "Administrador");

                st = conexionMySQL.createStatement();
                rs = st.executeQuery(sql);

               synchronized (devuelveDatos){
                    while (rs.next()) {
                        nombres.add(rs.getString("nombre"));
                        movimientos.add(rs.getInt("movimientos"));
                        dificultades.add(rs.getString("dificultad"));
                    }
                   devuelveDatos.notifyAll();
                }

            } catch (SQLException e) {
               e.printStackTrace();
           } finally{
               try {
                   conexionMySQL.close();
               } catch (SQLException e) {
                   e.printStackTrace();
               }
           }

        }).start();

        return nombres.isEmpty() && movimientos.isEmpty() && dificultades.isEmpty();
    }


    public void ejecutarUpdate(String sql) {
        nombres = new ArrayList<>();
        movimientos = new ArrayList<>();
        dificultades = new ArrayList<>();

        new Thread(() -> {
            try {
                this.conexionMySQL = DriverManager.getConnection("jdbc:mysql://192.168.1.129:3306/ranking?allowMultiQueries=true", "ivan", "Administrador");
                // AllowMultiQueries se emplea para poder ejecutar varias sentencias SQL encadenadas (suceso que ocurre cuando se inserta el registro de alguien que supera un récord)
                st = conexionMySQL.createStatement();
                st.executeUpdate(sql);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }).start();

    }

    public ArrayList<String> consultarDificultad(String dificultad) {

        datosJugador = new ArrayList<>();

        new Thread(() -> {
            // se debe hacer en un hilo por que si no lanza la excepción "android.os.NetworkOnMainThreadException"
            // por lo que, como bien dice la excepción, debemos crear un hilo que se ocupe de esta tarea y no el principal

            // https://stackoverflow.com/questions/6343166/how-to-fix-android-os-networkonmainthreadexception

            try  {

                this.conexionMySQL = DriverManager.getConnection("jdbc:mysql://192.168.1.129:3306/ranking", "ivan", "Administrador");

                PreparedStatement sentencia;
                sentencia = conexionMySQL.prepareStatement("SELECT * FROM records WHERE dificultad = ? ORDER BY posicion");
                sentencia.setString(1, dificultad);
                ResultSet rs = sentencia.executeQuery();

                synchronized (datosJugador){
                    while (rs.next()) {
                        datosJugador.add(rs.getString("nombre"));
                        datosJugador.add(rs.getString("movimientos"));
                    }
                    datosJugador.notifyAll();

                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally{
                try {
                    conexionMySQL.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }

        }).start();

        return datosJugador;
    }

    public ArrayList<String> getNombres() {
        return nombres;
    }

    public ArrayList<Integer> getMovimientos() {
        return movimientos;
    }

    public ArrayList<String> getDificultades() {
        return dificultades;
    }

}
