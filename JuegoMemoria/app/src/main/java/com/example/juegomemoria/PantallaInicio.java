package com.example.juegomemoria;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class PantallaInicio extends AppCompatActivity {
    Bundle opciones;
    int numTarjetas = 0;
    int l;
    int conta = 0;
    String dificultad, tema;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_inicio);


        final ListView lv = findViewById(R.id.idListView1);
        final ListView lv2 = findViewById(R.id.idListView2);
        final TextView difSel = findViewById(R.id.idSelectedDif);
        final TextView temaEscogido = findViewById(R.id.idTemaEscogido);
        final Button btnInicio = findViewById(R.id.idBtnInicio);

        btnInicio.setEnabled(false);
        btnInicio.setBackgroundColor(Color.GRAY);

        opciones = new Bundle(); // si reiniciamos y no inicializamos aquí dará error.

        lv.setOnItemClickListener((parent, view, position, id) -> {
           dificultad = ""+lv.getItemAtPosition(position);
           difSel.setText("Dificultad seleccionada: " + dificultad);
           String nivel = null; // por que "dificultad" incluye más texto
           switch(position){
               case 0:
                   numTarjetas = 8;
                   nivel = "Facil";
                   l = R.layout.activity_pantallajueegofacil;
                   break;
               case 1:
                   numTarjetas = 12;
                   nivel = "Normal";
                   l = R.layout.activity_pantallajueegonormal;
                   break;
               case 2:
                   numTarjetas = 16;
                   nivel = "Dificil";
                   l = R.layout.activity_pantallajueegodificil;
           }

            opciones.putInt("numTarjetas", numTarjetas);
            opciones.putString("nivel", nivel);
            opciones.putInt("layout", l);

            if (dificultad != null && tema != null){
                btnInicio.setEnabled(true);
                btnInicio.setBackground(getDrawable(R.drawable.botones_radio));
            }

        });

        lv2.setOnItemClickListener((parent, view, position, id) -> {
            tema = ""+lv2.getItemAtPosition(position);
            temaEscogido.setText("Tema seleccionado: " + tema);
            opciones.putString("tema", tema);

            if (dificultad != null && tema != null){
                btnInicio.setEnabled(true);
                btnInicio.setBackground(getDrawable(R.drawable.botones_radio));
            }
        });

    }

    public void iniciar(View v){
        Intent i = new Intent(this, PantallaJuego.class);
        i.putExtras(opciones);
        startActivity(i);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    public void salir(View v){
        System.exit(0);
    }


}

