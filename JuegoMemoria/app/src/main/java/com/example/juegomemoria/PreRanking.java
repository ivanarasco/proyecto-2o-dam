package com.example.juegomemoria;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

/**
 * @author Ivan Arasco Millán
 * @version 02/03/2020
 */

public class PreRanking extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pre_ranking);
    }

    /* Dependiendo del botón, la sentencia SQL para la BD va a devolver unos resultados
        u otros dependiendo de la dificultad elegida */

    private String sql;
    private Intent i;
    private Bundle consulta = new Bundle();

    public void elegirFacil(View v){
        sql = "SELECT * FROM records WHERE dificultad = 'Facil' ORDER BY posicion";
        i = new Intent(this, Ranking.class);
        consulta.putString("consulta", sql);
        consulta.putString("dificultad", "Dificultad elegida: Fácil");
        i.putExtras(consulta);
        startActivity(i);
    }
    public void elegirNormal(View v){
        sql = "SELECT * FROM records WHERE dificultad = 'Normal' ORDER BY posicion";
        i = new Intent(this, Ranking.class);
        consulta.putString("consulta", sql);
        consulta.putString("dificultad", "Dificultad elegida: Normal");
        i.putExtras(consulta);
        startActivity(i);
    }
    public void elegirDificil(View v){
        sql = "SELECT * FROM records WHERE dificultad = 'Dificil' ORDER BY posicion";
        i = new Intent(this, Ranking.class);
        consulta.putString("consulta", sql);
        consulta.putString("dificultad", "Dificultad elegida: Dificil");
        i.putExtras(consulta);
        startActivity(i);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }
    public void salir(View v){ // salir del programa

        System.exit(-1);
    }


}
