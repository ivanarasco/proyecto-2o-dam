package com.example.juegomemoria;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

/**
 * @author Ivan Arasco Millán
 * @version 02/03/2020
 *
 * La clase MainActivity representa la pantalla inicial, donde podremos iniciar el juego
 * así como salir de él y ver los Rankings.
 */
public class MainActivity extends AppCompatActivity {
    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity);
        btn = findViewById(R.id.btnRanking);

        /* consulta a la BD para ver los ranking disponibles*/

        try {
            Conexion c = new Conexion();

            String sql = "SELECT * FROM records";

            Boolean devuelveDatos = c.ejecutarSQL(sql);

             synchronized (devuelveDatos) {
                while (!devuelveDatos) {
                    try {
                        System.out.println("--- LOS ARRAYS ESTÁN VACÍOS... ESPERAMOS A QUE SE LLENEN --- ");
                        devuelveDatos.wait(); // para asegurarnos de que los datos del jugador quedan registrados en el ArrayList antes de recoger sus datos
                        if (!c.getNombres().isEmpty() && !c.getDificultades().isEmpty() && !c.getMovimientos().isEmpty()){
                            devuelveDatos = true;
                        }
                    }catch(InterruptedException e){ }
                }

            }

            System.out.println("--- YA SE HAN LLENADO LOS ARRAYS. DEBERÍAMOS TENER DATOS A ESTAS ALTURAS: --- " + devuelveDatos);

            if (devuelveDatos) {
                btn.setEnabled(true);
            } else {
                btn.setEnabled(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param v Empezar el juego.
     */
    public void iniciar(View v) {
        startActivity(new Intent(this, PantallaInicio.class));
    }

    /**
     * @param v Este método lee el archivo Records y muestra de quién es el record (nombre y movimientos)
     */
    public void mostrarRanking(View v) {
        startActivity(new Intent(this, PreRanking.class));
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    public void salir(View v) {
        System.exit(0);
    }
}
