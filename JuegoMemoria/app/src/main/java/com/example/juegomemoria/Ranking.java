package com.example.juegomemoria;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Ivan Arasco Millán
 * @version 02/03/2020
 *
 * Esta clase muestra el jugador leído del fichero Records (solo hay uno)
 */
public class Ranking extends AppCompatActivity  {
    private String consulta, dificultad;
    private TextView nombre, nombre2, nombre3, movimientos, movimientos2, movimientos3, difElegida;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);

        nombre = findViewById(R.id.idNombre);
        nombre2 = findViewById(R.id.idNombre2);
        nombre3 = findViewById(R.id.idNombre3);

        movimientos = findViewById(R.id.idMovimientos);
        movimientos2 = findViewById(R.id.idMovimientos2);
        movimientos3 = findViewById(R.id.idMovimientos3);

        difElegida = findViewById(R.id.dificultadElegida);

        iniciar();
    }

    private void iniciar(){

        try {
            consulta = getIntent().getExtras().getString("consulta");
            dificultad = getIntent().getExtras().getString("dificultad");
        } catch (NullPointerException e){
            Toast.makeText(this, "Ha ocurrido un error. Vuelva a iniciar la aplicación.", Toast.LENGTH_SHORT).show();
        }

        try {

            Conexion c = new Conexion();
            Boolean devuelveDatos = c.ejecutarSQL(consulta);

            synchronized (devuelveDatos) {
                while (!devuelveDatos) {
                    devuelveDatos.wait(); // para asegurarnos de que los datos del jugador quedan registrados en el ArrayList antes de recoger sus datos
                    if (!c.getNombres().isEmpty() && !c.getDificultades().isEmpty() && !c.getMovimientos().isEmpty()){
                        devuelveDatos = true;
                    }
                }
            }
            Thread.sleep(1000);
            System.out.println(c.getNombres());

            nombre.setText(c.getNombres().get(0));
            nombre2.setText(c.getNombres().get(1));
            nombre3.setText(c.getNombres().get(2));

            movimientos.setText(c.getMovimientos().get(0).toString());
            movimientos2.setText(c.getMovimientos().get(1).toString());
            movimientos3.setText(c.getMovimientos().get(2).toString());

            difElegida.setText(dificultad);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void salir(View v){
        System.exit(0);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

}
