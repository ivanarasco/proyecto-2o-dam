package com.example.juegomemoria;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

/**
 * @author Ivan Arasco Millán
 * @version 02/03/2020
 * <p>
 * Esta clase representa la lógica principal del juego memoria.
 * Por una parte reparte las parejas aleatoriamente, compara las distintas selecciones
 * realizadas y guarda el récord de movimientos en la base de datos del servidor.
 */

public class PantallaJuego extends AppCompatActivity {

    //Variables de clase.
    private ArrayList<Integer> tarjetas = new ArrayList<>();
    private ArrayList<Integer> aux = new ArrayList<>();
    private boolean continuar = true, disponible = true;
    private int contador = 0, movimientos = 0, parejasLogradas = 0;
    private ImageView img, anteriorImg = null;
    private EditText idNombreJgdrRec;
    private TextView msgVictoria;
    private Button salir, reiniciar, confirmar;
    private String nivel, tema;
    private Conexion c;
    private int numTarjetas, layout, movimientosJugador, movimientosJugador2, movimientosJugador3;

    private int[] imagenesTarjetas = { // los IDS de cada una de las tarjetas
            R.id.id1,
            R.id.id2,
            R.id.id3,
            R.id.id4,
            R.id.id5,
            R.id.id6,
            R.id.id7,
            R.id.id8,
            R.id.id9,
            R.id.id10,
            R.id.id11,
            R.id.id12,
            R.id.id13,
            R.id.id14,
            R.id.id15,
            R.id.id16,
    };

    private MediaPlayer bien, mal, victoria, spacesong, medievalsong; // Los sonidos de la aplicación

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        super.onCreate(savedInstanceState);

        try {
            numTarjetas = getIntent().getExtras().getInt("numTarjetas");
            nivel = getIntent().getExtras().getString("nivel");
            layout = getIntent().getExtras().getInt("layout");
            tema = getIntent().getExtras().getString("tema");
        } catch (NullPointerException e){
            Toast.makeText(this, "Ha ocurrido un error. Vuelva a iniciar la aplicación.", Toast.LENGTH_SHORT).show();
        }

        setContentView(layout);

        medievalsong = MediaPlayer.create(this, R.raw.medievalsong);
        spacesong = MediaPlayer.create(this, R.raw.spacesong);

        if (tema.equals("Medieval")) {
            findViewById(R.id.fondoPantalla).setBackgroundResource(R.drawable.fondocastillo);
            crearTarjetasMedieval();
            medievalsong.start();
        } else if (tema.equals("Planetas")){
            findViewById(R.id.fondoPantalla).setBackgroundResource(R.drawable.fondoespacio);
            crearTarjetasPlanetas();
            spacesong.start();
        } else {
            Toast.makeText(this, "Ocurrió un error al establecer el fondo.", Toast.LENGTH_SHORT).show();
            // En caso de que suceda algún tipo de problema escogiendo la temática,
            // el programa escoge la que utilicé para desarrollar la lógica en primera instancia.

            tarjetas.add(R.drawable.brown);
            tarjetas.add(R.drawable.brown);
            tarjetas.add(R.drawable.blue);
            tarjetas.add(R.drawable.blue);
            tarjetas.add(R.drawable.green);
            tarjetas.add(R.drawable.green);
            tarjetas.add(R.drawable.red);
            tarjetas.add(R.drawable.red);

            if (numTarjetas > 8){
                tarjetas.add(R.drawable.pink);
                tarjetas.add(R.drawable.pink);
                tarjetas.add(R.drawable.white);
                tarjetas.add(R.drawable.white);
            }
            if (numTarjetas > 12){
                tarjetas.add(R.drawable.yellow);
                tarjetas.add(R.drawable.yellow);
                tarjetas.add(R.drawable.black);
                tarjetas.add(R.drawable.black);
            }

        }

        // Inicializamos y ocultamos los elementos a utilizar.
        idNombreJgdrRec = findViewById(R.id.idNombreJgdrRec);
        msgVictoria = findViewById(R.id.idMsgVictoria);
        salir = findViewById(R.id.idBtnSalir);
        reiniciar = findViewById(R.id.idBtnVolver);
        confirmar = findViewById(R.id.idBtnConfirmar);

        msgVictoria.setVisibility(View.INVISIBLE);
        idNombreJgdrRec.setVisibility(View.INVISIBLE);
        confirmar.setVisibility(View.INVISIBLE);
        salir.setVisibility(View.INVISIBLE);
        reiniciar.setVisibility(View.INVISIBLE);

        bien = MediaPlayer.create(this, R.raw.bien);
        mal = MediaPlayer.create(this, R.raw.mal);
        victoria = MediaPlayer.create(this, R.raw.victoria);

        // Tras las declaraciones y asignaciones, iniciamos la repartición aleatoria.
        iniciar();

    } // onCreate

    private void crearTarjetasMedieval(){

        for (int i = 0 ; i < numTarjetas; i++){
           ImageView img = findViewById(imagenesTarjetas[i]);
           img.setImageResource(R.drawable.dorsomedieval);
        }

        tarjetas.add(R.drawable.castillo);
        tarjetas.add(R.drawable.castillo);
        tarjetas.add(R.drawable.caballero);
        tarjetas.add(R.drawable.caballero);
        tarjetas.add(R.drawable.casco);
        tarjetas.add(R.drawable.casco);
        tarjetas.add(R.drawable.cetro);
        tarjetas.add(R.drawable.cetro);

        if (numTarjetas > 8){
            tarjetas.add(R.drawable.corona);
            tarjetas.add(R.drawable.corona);
            tarjetas.add(R.drawable.escudo);
            tarjetas.add(R.drawable.escudo);
        }
        if (numTarjetas > 12){
            tarjetas.add(R.drawable.dinero);
            tarjetas.add(R.drawable.dinero);
            tarjetas.add(R.drawable.torre);
            tarjetas.add(R.drawable.torre);
        }

    }

    private void crearTarjetasPlanetas(){

        for (int i = 0 ; i < numTarjetas; i++){
            ImageView img = findViewById(imagenesTarjetas[i]);
            img.setImageResource(R.drawable.dorsoespacio);
        }

        tarjetas.add(R.drawable.planetone);
        tarjetas.add(R.drawable.planetone);
        tarjetas.add(R.drawable.planettwo);
        tarjetas.add(R.drawable.planettwo);
        tarjetas.add(R.drawable.planetthree);
        tarjetas.add(R.drawable.planetthree);
        tarjetas.add(R.drawable.planetfour);
        tarjetas.add(R.drawable.planetfour);

        if (numTarjetas > 8){
            tarjetas.add(R.drawable.planetfive);
            tarjetas.add(R.drawable.planetfive);
            tarjetas.add(R.drawable.planetsix);
            tarjetas.add(R.drawable.planetsix);
        }
        if (numTarjetas > 12){
            tarjetas.add(R.drawable.planetseven);
            tarjetas.add(R.drawable.planetseven);
            tarjetas.add(R.drawable.planeteight);
            tarjetas.add(R.drawable.planeteight);
        }

    }

    private void iniciar() {
        int i;
        int indiceColor;
        // desordenamos el array de tarjetas guardando de forma aleatoria en otro cada tarjeta.

        for (i = 0; i < numTarjetas; i++) {
            indiceColor = (int) (Math.random() * tarjetas.size()); // Cogemos un índice del arrayList de tarjetas
            aux.add(tarjetas.get(indiceColor)); // guardamos de forma desordana el elemento
            tarjetas.remove(indiceColor); // eliminamos ese color para no repetirlo
        }

    } // iniciar

    public void pulsar(View v) {
        if (disponible) { // para permitirle al usuario que siga pulsando. (2 visibles como máx).
            img = (ImageView) v; // elemento pulsado.
            int idImgSeleccionada = img.getId();

            img.setEnabled(false); // para que no puedas volver a pulsar la misma

            continuar = true;

            for (int i = 0; i < numTarjetas && continuar; i++) {
                if (idImgSeleccionada == imagenesTarjetas[i]) {
                    // Al no tener el índice, lo tenemos que buscar así.
                    // Y cuando coincida su resID, sabremos que estamos en el correcto y podremos girarlo.
                    img.setImageResource(aux.get(i)); // le damos la vuelta al elemento seleccionado
                    continuar = false; // para dejar de recorrer
                    contador++;
                }
            }

            if (anteriorImg == null) { // para el inicio de la partida.
                anteriorImg = img;
            }
            if (anteriorImg.getDrawable().getConstantState().equals(img.getDrawable().getConstantState()) && contador == 2) { // son parejas y funciona (La segunda vez, si coinciden)
                disponible = false; // hay que esperar a que termine de mostrarnos la pareja.
                v.postDelayed(() -> {
                    img.setVisibility(View.INVISIBLE);
                    anteriorImg.setVisibility(View.INVISIBLE);
                    disponible = true;
                }, 500);

                parejasLogradas++;
                contador = 0;
                anteriorImg.setEnabled(true);
                img.setEnabled(true);
                bien.start();
                movimientos++;

                comprobarVictoria(); // método para comprobar si ya hemos completado la partida.

            } else if (contador == 2) { // no lo son. (La segunda vez, si no coinciden)
                contador = 0;
                continuar = true;
                disponible = false;
                v.postDelayed(() -> {
                    if (tema.equals("Medieval")) {
                        img.setImageResource(R.drawable.dorsomedieval);         // volvemos al dorso de la última elegida
                        anteriorImg.setImageResource(R.drawable.dorsomedieval); // volvemos al dorso de la anterior (la primera)
                    } else if (tema.equals("Planetas")){
                        img.setImageResource(R.drawable.dorsoespacio);         // volvemos al dorso de la última elegida
                        anteriorImg.setImageResource(R.drawable.dorsoespacio); // volvemos al dorso de la anterior (la primera)
                    }
                    disponible = true;
                }, 500);

                img.setEnabled(true);
                anteriorImg.setEnabled(true);
                mal.start();
                movimientos++;
            } else { //if (contador == 1)
                anteriorImg = img;
            }


        }

    }

    private void comprobarVictoria() {
        if (parejasLogradas == numTarjetas/2) {
            //Thread.sleep(500); // medio segundo para que no se solapen los sonidos de acierto y victoria.
            victoria.start();
            msgVictoria.setVisibility(View.VISIBLE);

            // consultamos a la BD a ver si los movimientos realizados entran en el TOP 3.

            try {

                c = new Conexion();

                ArrayList<String> datosJugador = c.consultarDificultad(nivel);

                synchronized (datosJugador) {
                    while (datosJugador.isEmpty()) {
                        datosJugador.wait(); // para asegurarnos de que los datos del jugador quedan registrados en el ArrayList antes de recoger sus datos
                    }
                }

                movimientosJugador = Integer.parseInt(datosJugador.get(1));
                movimientosJugador2 = Integer.parseInt(datosJugador.get(3));
                movimientosJugador3 = Integer.parseInt(datosJugador.get(5));

                if (movimientos < movimientosJugador3 && movimientos != movimientosJugador && movimientos != movimientosJugador2){
                    // ha quedado entre los tres mejores (están ordenados y por tanto el tercero es el que más movimientos ha hecho,
                    // si el aspirante tiene menos y no el mismo número que los dos primeros, lo logró).
                    idNombreJgdrRec.setVisibility(View.VISIBLE);
                    confirmar.setVisibility(View.VISIBLE);
                } else {
                    salir.setVisibility(View.VISIBLE);
                    reiniciar.setVisibility(View.VISIBLE);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public void confirmar(View v) {
        if (idNombreJgdrRec.getText() != null) {
            String nombre = idNombreJgdrRec.getText().toString(); // recibimos el nombre

            try {
                c = new Conexion();

                String sql;

                if (movimientos < movimientosJugador) { // es mejor que el primero
                    // id = 1
                    // mover primero a segundo
                    // mover segundo a tercero
                    // eliminar tercero

                    sql =   "DELETE FROM records WHERE posicion = 3 AND dificultad = '" + nivel+ "';\n" + // Eliminamos el tercero
                            "UPDATE records SET posicion = 3 WHERE posicion = 2 AND dificultad = '" + nivel+ "';\n" + // convertimos el segundo en tercero
                            "UPDATE records SET posicion = 2 WHERE posicion = 1 AND dificultad = '" + nivel+ "';\n" + // convertimos el primero en segundo

                            "INSERT INTO records (nombre, movimientos, dificultad, posicion) VALUES ('"+nombre+"',"+ movimientos+",'"+nivel+"',"+ 1+");"; // añadimos el primero

                    c.ejecutarUpdate(sql);

                } else if (movimientos < movimientosJugador2) { // es mejor que el segundo
                    // id = 2
                    // mover segundo a tercero
                    // eliminar tercero
                    sql =   "DELETE FROM records WHERE posicion = 3 AND dificultad = '" + nivel+ "';\n" + // Eliminamos el tercero
                            "UPDATE records SET posicion = 3 WHERE posicion = 2 AND dificultad = '" + nivel+ "';\n" + // convertimos el segundo en tercero
                            "INSERT INTO records (nombre, movimientos, dificultad, posicion) VALUES ('"+nombre+"',"+ movimientos+",'"+nivel+"',"+ 2+");"; // añadimos el segundo

                    c.ejecutarUpdate(sql);

                } else if (movimientos < movimientosJugador3) { // es mejor que el tercero
                    // id = 3
                    // eliminar tercero
                    sql =   "DELETE FROM records WHERE posicion = 3 AND dificultad = '" + nivel+ "';\n" + // Eliminamos el tercero
                            "INSERT INTO records (nombre, movimientos, dificultad, posicion) VALUES ('"+nombre+"',"+ movimientos+",'"+nivel+"',"+ 3+");"; // añadimos el tercero

                    c.ejecutarUpdate(sql);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            /* Y en caso de ser uno de los tres mejores, sustituirlo por la posición cuyo número es mejor. */

            idNombreJgdrRec.setVisibility(View.INVISIBLE);
            confirmar.setVisibility(View.INVISIBLE);

            salir.setVisibility(View.VISIBLE);
            reiniciar.setVisibility(View.VISIBLE);

        } else {
            Toast.makeText(this, "Introduce nombre para confirmar.", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // cuando "se echa" hacia atrás y se pare la música.
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (tema.equals("Medieval")) {
                medievalsong.stop();
            } else if (tema.equals("Planetas")) {
                spacesong.stop();
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    public void reiniciar(View v) { // iniciar otra partida y reiniciar las canciones.

        if (tema.equals("Medieval")) {
            medievalsong.stop();
        } else if (tema.equals("Planetas")) {
            spacesong.stop();
        }

        startActivity(new Intent(this, PantallaInicio.class));

    }

    public void salir(View v){ // salir del programa

        System.exit(-1);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // para ocultar la barra de navegación inferior
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }


}